function distance(first, second){
	if((!Array.isArray(first)) || (!Array.isArray(second))){
		throw "Invalid Type!";
	}
	
	if(first.length == 0 && second.length == 0){
		return 0;
	}
	else{
		for(var i=0; i<first.length; i++){
			for(var j=i+1; j<first.length ; j++){
				if(first[i] == first[j]){
					first.splice(j,1);
				}
			}
		}
	
		for(var k=0; k<second.length; k++){
			for(var l=k+1; l<second.length; l++){
				if(second[k] == second[l]){
					second.splice(l,1);
				}
			}
		}
	
		let dist = 0;
		for(var	i=0; i<first.length; i++){
			var j = 0;
			while(first[i] != second[j] && j<second.length){
				j++;
			}
			if(j == second.length){
				dist++;
			}
		}
	
		for(var k=0; k<second.length; k++){
			var l = 0;
			while(second[k] != first[l] && l<first.length){
				l++;
			}
			if(l== first.length){
				dist++;
			}
		}
	
		return dist;
		}
}


module.exports.distance = distance